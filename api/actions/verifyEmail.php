<?php

header('Content-Type: application/json');
include_once '../config/Database.php';

$orderid = $json['orderId'];
$accessToken = $json['accessToken'];
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://api-m.paypal.com/v2/checkout/orders/'.$orderid.'/capture');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Content-Type: application/json',
    'Authorization: Bearer '.$accessToken,
]);
curl_setopt($ch, CURLOPT_POSTFIELDS, '');
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

$result = curl_exec($ch);
$err = curl_error($ch);

$status="";
if ($err) {
    $result["success"] = false;
    $result["error"] = "cURL Error #:".$err;
    $result = json_encode($result);
}

$result = json_decode($result, true);
$success = $result->purchase_units[0]->payments->captures[0]->status;

if ($success == "COMPLETED") {
    $result["success"] = true;
}
else if ($success == "DECLINED") {
    $result["success"] = false;
    $result["error"] = "Payment annulé par l'utilisateur";
}
else {
    $result["success"] = false;
    $result["error"] = "Prenez contact avec Paypal pour l'erreur";
}

echo json_encode($result);

?>                                                                                                                                                                                                                                                                                                                                                                   